"""datanalyyzer calss"""
class DataAnalyzer:

    """getting results """

    def getresult( self,data, maximum_col, minimum_col, name_col):
        """getting results """
        result = ""
        count = float(data[0][maximum_col]) - float(data[0][minimum_col])

        for eachitem in data:
            difference = abs(float(eachitem[maximum_col]) - float(eachitem[minimum_col]))
            if difference <= count:
                count = difference
                result = eachitem[name_col]
                # print(count, result)
        return result
