"""_summary"""
import csv

class DataExtractor:
    """summary"""
    def rawdata(self, filename):
        """docstring"""
       
        with open(filename, "r", encoding = "UTF-8") as file:
            csvreader = csv.DictReader(file)
            data = list(csvreader)
        return data

        