""" imported module"""
import DataExtractor
import DataAnalyzer

class Football :
    """ class football"""
    footballcsv = "/home/vinaykumar/Desktop/solid/Data/football.csv"
    dataobject = DataExtractor.DataExtractor()
    data = dataobject.rawdata(footballcsv)

    footballMatch = DataAnalyzer.DataAnalyzer()

    nameOfTheTeamWithTheSmallestDifference = footballMatch.getresult(data, "F", "A", "Team")
    print(f"Name of the team with the smallest difference is  {nameOfTheTeamWithTheSmallestDifference}")