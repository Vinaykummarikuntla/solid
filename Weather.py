"""summary"""
import DataExtractor
import DataAnalyzer

class Weather:
    """summary"""

    weatherCSV = "/home/vinaykumar/Desktop/solid/Data/weather.csv"
    dataobject = DataExtractor.DataExtractor()

    data = dataobject.rawdata(weatherCSV)

    weatherday = DataAnalyzer.DataAnalyzer()
    smallestTemperatureSpread = weatherday.getresult(data, "Max", "Min", "Day")

    print(f'The day number with the smallest temperature spread is day {smallestTemperatureSpread}')